const fs2 = require('fs');
const http = require('http');
const exec = require('child_process').exec;
const index = fs2.readFileSync(__dirname + '/index.html');
const dotenv = require("dotenv").config();

//File path
const fileName = "logger.txt";

// Updating text in file
require('./writelogs').writeLogs();

const app = http.createServer(function(req, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end(index);
});


const io = require('socket.io').listen(app);

io.on('connection', function(socket) {

    fs2.open(fileName, 'r', function(err) {

        exec("tail " + fileName, function(err, tailLogs, stderr) {
            tailLogs = tailLogs.replace(new RegExp('\n','g'), '<br />');
            if(!tailLogs){
                tailLogs = "<h3>File is Empty</h3>"
            }
            socket.emit('logsSync', { message: tailLogs, id: socket.id  });
        });

        fs2.watch(fileName, function(event) {
            if(event === "change") {
                exec("tail " + fileName, function(err, tailLogs, stderr) {
                    tailLogs = tailLogs.replace(new RegExp('\n','g'), '<br />');
                    if(!tailLogs){
                        tailLogs = "<h3>File is Empty</h3>"
                    }
                    socket.emit('logsSync', { message: tailLogs, id: socket.id  });
                });
            }
        });
    });
});

// working......................
// const readLastLines = require('read-last-lines');
// app.get('/', (request, response)=>{
//     // // http://localhost:3001/?line=5
//     // var readNumberOfLines = parseInt(Object.values(request.query)) || 10;
//     // console.log(readNumberOfLines);
//     // readLastLines.read('test.txt', readNumberOfLines)
//     //     .then((lines) =>{
//     //         console.log(lines);
//     //         response.send(lines);
//     //     });          
// });

const port = process.env.PORT;
console.log(port);
app.listen( port|3001, error=>{
    if(!error)
        return console.log(`server running `);
    console.log(`${error}`);
});