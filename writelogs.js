const fs = require("fs");
const ws = fs.createWriteStream('logger.txt', {flags : "a"});
const randomstring = require("randomstring");
module.exports = {
    writeLogs: ()=>{
        setInterval(() => {
            ws.write(JSON.stringify({ log_time: new Date()}) + "\n");
         }, 10000);
    }
};